Compilation instruction

## Install 'Python 2.7.16' to 'C:\' (https://www.python.org/downloads/release/python-2716/)
   the following directory will be generated:
   C:\Python27

## Copy 'LLVM' directory to 'C:\:', the following directory will be generated (LLVM was included in quectel package):
   C:\LLVM\4.0.3
	
## Install 'Visual Studio Code', version used in this project is 1.46.1

## Download 'uart' project to a location in 'C:\'. 

## Open 'Visual Studio Code', make sure 'Explorer' is selected (if not: click 'View' -> 'Explorer' or 'Ctrl'+'Shift'+'E').
## Under 'Explorer', in second pane under 'explorer', select 'add folder'. 
## in the file manager window, browse to 'uart' project downloaded location and selected 'uart' directory and click 'add',
## after 'uart' is added to workspace, click 'Terminal' -> 'Run Build Task' or 'Ctrl'+'Shift'+'B', build starts.


## when "Output Binary Format: bin\program.bin Bit Width: 8, Bank: 1" is printed, the compilation is successful.
 - the following three files will be generated in 'uart/bin' automatically:
   oem_app_path.ini
   program.bin
   quectel_demo_uart.elf

## if "could not open port 'COM10' is printed, check the Quectel USB DMPORT # in device manager,
   if it is not COM10, change the 'COM10' to correct COM# in "upload" function in "uart/.vscode/QDL95.py'.
  